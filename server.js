// import modules
const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');
const colors = require('colors');
const errorHandler = require('./src/lib/middlewares/error');
const path = require('path');

// load environment variables
dotenv.config();

// construct app
const app = express();

// middlewares
app.use(express.static(path.join(__dirname, 'src/lib/public')))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));

// routes
app.use(errorHandler);

// app.get('/', (_, res) => {
//   res.json({ code: 200, message: 'hello from local api vra site.' });
// });

const baseURL = '/api';
const vidmURL = '/vidm/api';
const usersRouter = require('./src/components/users/users_router');
const catalogsRouter = require('./src/components/catalogs/catalogs_router');
const deploymentsRouter = require('./src/components/deployments/deployments_router');
const projectsRouter = require('./src/components/projects/projects_router');
const blueprintsRouter = require('./src/components/blueprints/blueprint_router');
const iaasRouter = require('./src/components/iaas/iaas_router');
const approvalRouter = require('./src/components/approvals/approvals_router');
const projectServiceRouter = require('./src/components/project-services/projectService_router');
// const vidmRouter = require('./src/components/vidm/vidm_routes');

app.use(baseURL, usersRouter);
app.use(baseURL, catalogsRouter);
app.use(baseURL, deploymentsRouter);
app.use(baseURL, projectsRouter);
app.use(baseURL, blueprintsRouter);
app.use(baseURL, iaasRouter);
app.use(baseURL, approvalRouter);
app.use(baseURL, projectServiceRouter);
// app.use(baseURL, vidmRouter);

// listening
const PORT = process.env.PORT || 1337;
const NODE_ENV = process.env.NODE_ENV;

app.listen(PORT, () => {
  console.log(`server running in ${NODE_ENV} mode on port ${PORT}`.magenta);
  console.log(`the url ${process.env.URL}`);
});

// handle unhandled promise rejection
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`);

  // close server & exit process
  server.close(() => process.exit(1));
});
