const axios = require('axios');
const URL = `${process.env.URL}/catalog/api/items`;
const queryString = require('querystring');
const ErrorResponse = require('../../lib/helpers/errorResponse');

exports.getAllCatalogs = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${URL}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/catalogs - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getCatalogById = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(`${URL}/${id}?${queryString.stringify(req.query)}`, option);
    console.log('/api/catalogs/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }

};

exports.postRequest = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
      'Content-Type': 'application/json',
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.post(`${URL}/${id}/request`, payload, option);
    console.log('POST /api/catalogs/:id/request - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getAllVersion = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(`${URL}/${id}/versions`, option);
    console.log('POST /api/catalogs/:id/versions - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getAllVersionById = async (req, res) => {
  const { authorization } = req.headers;
  const { id, versionId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${URL}/${id}/versions/${versionId}`,
      option
    );
    console.log('POST /api/catalogs/:id/versions/:versionId - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }

};

// >>> Content & Policies Menu <<< //

/** Content Source */
exports.getContentSources = async (req, res, next) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/catalog/api/admin/sources?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/catalogs/admin/sources?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }

};

exports.getContentSourceById = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/catalog/api/admin/sources/${id}?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/catalogs/admin/sources/:id?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.createContentSource = async (req, res, next) => {
  const { authorization } = req.headers;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.post(
      `${process.env.URL}/catalog/api/admin/sources`,
      payload,
      option
    );
    console.log(`POST /api/catalogs/admin/sources - masuk pak eko!`);
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.deteleContentSource = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.delete(
      `${process.env.URL}/catalog/api/admin/sources/${id}`,
      option
    );
    console.log(`DELETE /api/catalogs/admin/sources/:id - masuk pak eko!`);
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

/** Catalog Entitlements (Content Sharing) */
exports.getEntitlements = async (req, res, next) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/catalog/api/admin/entitlements?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/catalog/api/admin/entitlements?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.createEntitlements = async (req, res, next) => {
  const { authorization } = req.headers;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.post(
      `${process.env.URL}/catalog/api/admin/entitlements`,
      payload,
      option
    );
    console.log(`POST /api/catalog/api/admin/entitlements - masuk pak eko!`);
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.deleteEntitlements = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.delete(
      `${process.env.URL}/catalog/api/admin/entitlements/${id}`,
      option
    );
    console.log(
      `DELETE /api/catalog/api/admin/entitlements/${id} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

/** Content */
exports.getAdminItem = async (req, res, next) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/catalog/api/admin/items?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/catalog/api/admin/item?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getAdminItemById = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/catalog/api/admin/items/${id}?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/catalog/api/admin/item?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};
