const router = require('express').Router();
const {
  getAllCatalogs,
  getCatalogById,
  postRequest,
  getAllVersion,
  getAllVersionById,
  getContentSources,
  getContentSourceById,
  createContentSource,
  deteleContentSource,
  getEntitlements,
  createEntitlements,
  deleteEntitlements,
  getAdminItem,
  getAdminItemById,
} = require('./catalogs_controller');

router.get('/catalogs', getAllCatalogs);
router.get('/catalogs/:id', getCatalogById);
router.post('/catalogs/:id/request', postRequest);
router.get('/catalogs/ref/:id/versions', getAllVersion);
router.get('/catalogs/ref/:id/versions/:versionId', getAllVersionById);
router.get('/catalogs/admin/sources', getContentSources);
router.get('/catalogs/admin/sources/:id', getContentSourceById);
router.post('/catalogs/admin/sources', createContentSource);
router.delete('/catalogs/admin/sources/:id', deteleContentSource);
router.get('/catalogs/admin/entitlements', getEntitlements);
router.post('/catalogs/admin/entitlements', createEntitlements);
router.delete('/catalogs/admin/entitlements/:id', deleteEntitlements);
router.get('/catalogs/admin/items', getAdminItem);
router.get('/catalogs/admin/items/:id', getAdminItemById);

module.exports = router;
