const router = require('express').Router();
const {
  getAllDeployments,
  getDeploymentById,
  getActions,
  getActionsById,
  getResources,
  getResourceById,
  getEvents,
  getAllActionsByResource,
  getActionsByResource,
  postDeployment,
  postActionsByResources,
  updateDeployment,
  cancelAction,
  dismissAction,
  filterProject,
  filterStatus,
  filterCloudTypes,
  filterTags,
  filterCloudAccounts,
  filterRequestedBy,
  filterResourceTypes
} = require('./deployments_controller');

router.get('/deployments', getAllDeployments);
router.get('/deployments/:id', getDeploymentById);
router.get('/deployments/:id/actions', getActions);
router.get('/deployments/:id/actions/:actionId', getActionsById);
router.get('/deployments/:id/resources', getResources);
router.get('/deployments/:id/resources/:resourceId', getResourceById);
router.get('/deployments/:id/events', getEvents);
router.get(
  '/deployments/:id/resources/:resourceId/actions',
  getAllActionsByResource
);
router.get(
  '/deployments/:id/resources/:resourceId/actions/:actionId',
  getActionsByResource
);
router.post('/deployments/:id/requests', postDeployment);
router.post(
  '/deployments/:id/resources/:resourceId/requests',
  postActionsByResources
);
router.patch('/deployments/:id', updateDeployment);
router.post('/deployments/requests/:id', cancelAction);
router.get('/deployments/requests/:id', dismissAction);

router.get('/deployments/filters/projects', filterProject);
router.get('/deployments/filters/status', filterStatus);
router.get('/deployments/filters/cloudTypes', filterCloudTypes);
router.get('/deployments/filters/resourceTypes', filterResourceTypes);
router.get('/deployments/filters/tags', filterTags);
router.get('/deployments/filters/cloudAccounts', filterCloudAccounts);
router.get('/deployments/filters/requestedBy', filterRequestedBy);

module.exports = router;
