const axios = require('axios');
const URL = `${process.env.URL}/deployment/api/deployments`;
const queryString = require('querystring');
// const Pusher = require('pusher');

// const pusher = new Pusher({
//   appId: process.env.PUSHER_APP_ID,
//   key: process.env.PUSHER_APP_KEY,
//   secret: process.env.PUSHER_APP_SECRET,
//   cluster: process.env.PUSHER_APP_CLUSTER,
//   useTLS: true,
// });

exports.getAllDeployments = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/deployments - masuk pak eko!');
    // const urlComplete = `${process.env.URL}/deployment/api/deployments?size=10&sort=createdAt%2ClastUpdatedAt%2CDESC&search=&expandProject=true&expandResources=true&expandLastRequest=true&forceCachedResources=true`;
    // setInterval(async () => {
    //   try {
    //     const { data } = await axios.get(urlComplete, option);
    //     pusher.trigger('vra-channel', 'vra-event', {
    //       ...data
    //     });
    //   } catch (error) {
    //     console.error(error.message);
    //   }
    // }, 5000);
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getDeploymentById = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/deployment/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getActions = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/${id}/actions`, option);
    console.log('/api/deployments/:id/actions - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getActionsById = async (req, res) => {
  const { authorization } = req.headers;
  const { id, actionId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/${id}/actions/${actionId}`, option);
    console.log('/api/deployments/:id/actions/:actionId - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getResources = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/${id}/resources`, option);
    console.log('/api/deployments/:id/resources - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getResourceById = async (req, res) => {
  const { authorization } = req.headers;
  const { id, resourceId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/resources/${resourceId}`,
      option
    );
    console.log(
      `/api/deployments/${id}/resources/${resourceId} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getEvents = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/events?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/deployments/:id/events - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getAllActionsByResource = async (req, res) => {
  const { authorization } = req.headers;
  const { id, resourceId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/resources/${resourceId}/actions`,
      option
    );
    console.log(
      '/api/deployments/:id/resources/:resourceId/actions - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getActionsByResource = async (req, res) => {
  const { authorization } = req.headers;
  const { id, resourceId, actionId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/resources/${resourceId}/actions/${actionId}`,
      option
    );
    console.log(
      '/api/deployments/:id/resources/:resourceId/actions/:actionId - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.postDeployment = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
      'Content-Type': 'application/json',
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.post(`${URL}/${id}/requests`, payload, option);
    console.log('POST /api/deployments/:id/requests - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.postActionsByResources = async (req, res) => {
  const { authorization } = req.headers;
  const { id, resourceId } = req.params;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
      'Content-Type': 'application/json',
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.post(
      `${URL}/${id}/resources/${resourceId}/requests`,
      payload,
      option
    );
    console.log(
      'POST /api/deployments/:id/resources/:resourceId/requests - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.updateDeployment = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const payload = req.body;
  const options = {
    headers: {
      'Content-Type': 'application/json',
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.patch(`${URL}/${id}`, payload, options);
    console.log('PATCH /api/deployments/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.cancelAction = async (req, res) => {
  const { authorization } = req.headers;
  const payload = req.body;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.post(
      `${process.env.URL}/deployment/api/requests/${id}?${queryString.stringify(
        req.query
      )}`,
      payload,
      option
    );

    console.log(
      'POST /api/deployments/requests/:id?actions=cancel - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.dismissAction = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL}/deployment/api/requests/${id}?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      'GET /api/deployments/requests/:id?actions=dismiss - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/projects?page=0&size=10&apiVersion=2020-08-25
exports.filterProject = async (req, res, next) => {
  const url = `${URL}/filters/projects?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/projects - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/status?page=0&size=10&apiVersion=2020-08-25
exports.filterStatus = async (req, res, next) => {
  const url = `${URL}/filters/status?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/status - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/cloudTypes?page=0&size=10&apiVersion=2020-08-25
exports.filterCloudTypes = async (req, res, next) => {
  const url = `${URL}/filters/cloudTypes?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/cloudTypes - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/resourceTypes?page=0&size=10&apiVersion=2020-08-25
exports.filterResourceTypes = async (req, res, next) => {
  const url = `${URL}/filters/resourceTypes?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/resourceTypes - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/tags?page=0&size=10&apiVersion=2020-08-25
exports.filterTags = async (req, res, next) => {
  const url = `${URL}/filters/tags?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/tags - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/cloudAccounts?page=0&size=10&apiVersion=2020-08-25
exports.filterCloudAccounts = async (req, res, next) => {
  const url = `${URL}/filters/cloudAccounts?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/cloudAccounts - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

// https://vra.nashta.local/deployment/api/deployments/filters/requestedBy?page=0&size=10&apiVersion=2020-08-25
exports.filterRequestedBy = async (req, res, next) => {
  const url = `${URL}/filters/requestedBy?${queryString.stringify(req.params)}`;
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(url, option);
    console.log(
      'GET /api/deployments/filters/requestedBy - masuk pak eko!'
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};