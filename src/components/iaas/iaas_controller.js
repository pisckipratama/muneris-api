const axios = require('axios');
const URL = `${process.env.URL}/iaas/api`;
const asyncHandler = require('../../lib/middlewares/asyncHandler');

exports.getFlavors = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/flavors`, option);
    console.log('/api/iaas/flavors - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getImages = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/images`, option);
    console.log('/api/iaas/images - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};
