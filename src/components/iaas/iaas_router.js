const router = require('express').Router();
const { getFlavors, getImages } = require('./iaas_controller');

router.get('/iaas/flavors', getFlavors);
router.get('/iaas/images', getImages);

module.exports = router;
