const router = require('express').Router();
const {
  getAllBlueprints,
  getBlueprintById,
  getVersionByBlueprint,
  getVersionByBlueprintById,
  getResourcePlan,
  getBluperintRequest,
  postBluprintActionCancel,
  postBluprintRequest,
  getBluprintRequestById,
  deleteBluprintRequestById,
  getBluprintRequestPlanById
} = require('./blueprint_controller');

router.get('/blueprints', getAllBlueprints);
router.get('/blueprints/:id', getBlueprintById);
router.get('/blueprints/:id/versions', getVersionByBlueprint);
router.get('/blueprints/:id/versions/:versionId', getVersionByBlueprintById);
router.get('/blueprint-requests/:id/resources-plan', getResourcePlan);
router.get('/blueprint-requests', getBluperintRequest);
router.get('/blueprint-requests/:id', getBluprintRequestById);
router.delete('/blueprint-requests/:id', deleteBluprintRequestById);
router.post('/blueprint-requests', postBluprintRequest);
router.post('/blueprint-requests/:id/actions/cancel', postBluprintActionCancel);
router.get('/blueprint-requests/:id/plan', getBluprintRequestPlanById);

module.exports = router;
