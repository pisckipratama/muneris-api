const axios = require('axios');
const URL = `${process.env.URL}/blueprint/api/blueprints`;
const queryString = require('querystring');

exports.getAllBlueprints = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/blueprints - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getBlueprintById = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };
  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(`${URL}/${id}`, option);
    console.log('/api/blueprints/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getVersionByBlueprint = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/versions?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/blueprints/:id/versions - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getVersionByBlueprintById = async (req, res) => {
  const { authorization } = req.headers;
  const { id, versionId } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}/versions/${versionId}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/blueprints/:id/versions/:versionId - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getResourcePlan = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };
  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL
      }/blueprint/api/blueprint-requests/${id}/resources-plan?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/blueprints/blueprint-requests/${id}/resources-plan?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getBluperintRequest = async (req, res, next) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };
  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${process.env.URL
      }/blueprint/api/blueprint-requests?${queryString.stringify(
        req.query
      )}`,
      option
    );
    console.log(
      `/api/blueprints/blueprint-requests?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.postBluprintRequest = async (req, res, next) => {
  const { authorization } = req.headers;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
      'Content-Type': 'application/json',
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.post(`${process.env.URL}/blueprint/api/blueprint-requests?${queryString.stringify(
      req.query
    )}`, payload, option);
    console.log('POST /api/blueprint-requests - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.postBluprintActionCancel = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
      'Content-Type': 'application/json',
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.post(`${process.env.URL}/blueprint/api/blueprint-requests/${id}/actions/cancel?${queryString.stringify(
      req.query
    )}`, payload, option);
    console.log('POST /api/blueprint-requests/:id/actions/cancel - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.deleteBluprintRequestById = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.delete(`${process.env.URL}/blueprint/api/blueprint-requests/${id}?${queryString.stringify(
      req.query
    )}`, option);
    console.log('DELETE /api/blueprint-requests/:id - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getBluprintRequestById = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };
  const url = `${process.env.URL}/blueprint/api/blueprint-requests/${id}?${queryString.stringify(
    req.query
  )}`;
  console.log(url)

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(url, option);
    console.log('/api/blueprint-requests/:id - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getBluprintRequestPlanById = async (req, res, next) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };
  const url = `${process.env.URL}/blueprint/api/blueprint-requests/${id}/plan?${queryString.stringify(
    req.query
  )}`;
  console.log(url)

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(url, option);
    console.log('/api/blueprint-requests/:id/plan - masuk pak eko!');
    return res.status(200).json(data);

  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};