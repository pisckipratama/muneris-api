const axios = require('axios');
const URL = `${process.env.URL}/approval/api/approvals`;
const queryString = require('querystring');

exports.getAllApproval = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/approvals - masuk pak eko!');
    return res.status(200).json(data);
  } catch {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.getApprovalById = async (req, res) => {
  const { authorization } = req.headers;
  const { id } = req.params;
  const option = {
    headers: {
      authorization,
    },
  };
  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(
      `${URL}/${id}?${queryString.stringify(req.query)}`,
      option
    );
    console.log('/api/approvals/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.postApproval = async (req, res) => {
  const { authorization } = req.headers;
  const payload = req.body;
  const option = {
    headers: {
      authorization,
    },
  };
  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.post(`${URL}/action`, payload, option);
    console.log('/api/approvals/:id - masuk pak eko!');
    return res.status(200).json(data);
  } catch {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};
