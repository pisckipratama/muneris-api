const router = require('express').Router();
const {
  getAllApproval,
  getApprovalById,
  postApproval,
} = require('./approvals_controller');

router.get('/approvals', getAllApproval);
router.get('/approvals/:id', getApprovalById);
router.post('/approvals/action', postApproval);

module.exports = router;
