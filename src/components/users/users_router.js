const router = require('express').Router();
const {
  loginUser,
  forgotPassword,
  searchGroup,
  orgs,
  orgsId,
  orgsListUsers,
  getRole,
  patchRole,
  getServiceRoles,
  patchServiceRole,
  getActiveUsers,
  getTotalResultGroups,
  getRolesGroups,
  patchOrgsRole,
  getSession,
  getUserInfo,
  getProfile,
  getLoggedInUser
} = require('./users_controller');

router.post('/user/login', loginUser);
router.post('/user/forgotpassword', forgotPassword);
router.get('/groups/search', searchGroup);
router.get('/user/loggedin/orgs', orgs);
router.get('/orgs/:orgId', orgsId);
router.get('/orgs/:orgId/users', orgsListUsers);
router.get('/user/:userId/orgs/:orgId/roles', getRole);
router.patch('/user/:userId/orgs/:orgId/roles', patchRole);
router.get('/user/:userId/orgs/:orgId/service-roles', getServiceRoles);
router.patch('/user/:userId/orgs/:orgId/service-roles', patchServiceRole);
router.get('/v2/orgs/:orgId/users', getActiveUsers);
router.get('/orgs/:orgId/groups', getTotalResultGroups);
router.get('/orgs/:orgId/roles', getRolesGroups);
router.patch('/orgs/:orgId/roles', patchOrgsRole);
router.get('/provisioning/uerp/provisioning/auth/session', getSession);
router.get('/users/:userId/orgs/:orgId/info', getUserInfo);
router.get('/user/loggedin/profile', getProfile);
router.get('/user/loggedin', getLoggedInUser);

module.exports = router;
