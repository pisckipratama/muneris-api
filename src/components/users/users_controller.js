const axios = require('axios');
const url = `${process.env.URL}/csp/gateway/am/api/login`;
const asyncHandler = require('../../lib/middlewares/asyncHandler');
const qs = require('querystring');
const ErrorResponse = require('../../lib/helpers/errorResponse');

exports.loginUser = async (req, res) => {
  const payload = req.body;
  const options = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const { data } = await axios.post(url, payload, options);
    console.log('/api/users/login - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.forgotPassword = async (req, res) => {
  const payload = req.body;
  const url = `${process.env.VIDM_URL}/SAAS/jersey/manager/api/scim/ForgotPasswordRequests`;
  const options = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const { data } = await axios.post(url, payload, options);
    console.log('/vidm/api/forgot-password-requests - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};

exports.searchGroup = async (req, res, next) => {
  const url = `${process.env.URL}/csp/gateway/am/api/groups/search?${qs.stringify(req.query)}`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/groups/search - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.orgs = async (req, res, next) => {
  const url = `${process.env.URL}/csp/gateway/am/api/loggedin/user/orgs`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);

    let refLinksNew = data.refLinks.map(el => {
      let id = el.split('/');
      el = `/api/orgs/${id[6]}`
      return el
    });
    data.refLinks = refLinksNew;

    console.log('/api/user/loggedin/orgs - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.orgsId = async (req, res, next) => {
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/orgs/${orgId}`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    let splitRefLink = data.refLink.split('/');
    data.refLink = `/api/orgs/${splitRefLink[6]}`;

    console.log('/api/user/orgs/:orgId - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.orgsListUsers = async (req, res, next) => {
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/orgs/${orgId}/users`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/orgs/:orgId/users - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.getRole = async (req, res, next) => {
  const { userId, orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/users/${userId}/orgs/${orgId}/roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    data.map(item => {
      let id = item.refLink.split('/');
      item.id = id[8];
      return item;
    })

    console.log('/api/user/:userId/orgs/:orgId/roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.patchRole = async (req, res, next) => {
  const payload = req.body;
  const { userId, orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/users/${userId}/orgs/${orgId}/roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.patch(url, payload, options);
    console.log('PATCH /api/user/:userId/orgs/:orgId/roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.getServiceRoles = async (req, res, next) => {
  const { userId, orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/users/${userId}/orgs/${orgId}/service-roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/user/:userId/orgs/:orgId/service-roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.patchServiceRole = async (req, res, next) => {
  const payload = req.body;
  const { userId, orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/users/${userId}/orgs/${orgId}/service-roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.patch(url, payload, options);
    console.log('PATCH /api/user/:userId/orgs/:orgId/service-roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.getActiveUsers = async (req, res, next) => {
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/portal/api/v2/orgs/${orgId}/users?${qs.stringify(req.params)}`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/v2/orgs/:orgId/users - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.getTotalResultGroups = async (req, res, next) => {
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/portal/api/orgs/${orgId}/groups?${qs.stringify(req.params)}`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/portal/orgs/:orgId/groups - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.getRolesGroups = async (req, res, next) => {
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/portal/api/orgs/${orgId}/roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/portal/orgs/:orgId/roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

exports.patchOrgsRole = async (req, res, next) => {
  const payload = req.body;
  const { orgId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/orgs/${orgId}/roles`;
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.patch(url, payload, options);
    console.log('PATCH /api/orgs/:orgId/roles - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

// https://vra.nashta.local/provisioning/uerp/provisioning/auth/session
exports.getSession = async (req, res, next) => {
  const url = `${process.env.URL}/provisioning/uerp/provisioning/auth/session`;
  console.log(url);
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log('/api/provisioning/uerp/provisioning/auth/session - masuk pak eko!');
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

// https://vra.nashta.local/csp/gateway/am/api/users/20883bce-06fe-456c-9dfd-0dc82988dae1/orgs/620ced4f-478a-4681-adb7-afb7dd2af152/info
exports.getUserInfo = async (req, res, next) => {
  const { orgId, userId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/users/${userId}/orgs/${orgId}/info`;
  console.log(url);
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log(`/api/users/${userId}/orgs/${orgId}/info - masuk pak eko!`);
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

// https://vra.nashta.local/csp/gateway/am/api/loggedin/user/profile
exports.getProfile = async (req, res, next) => {
  const { orgId, userId } = req.params;
  const url = `${process.env.URL}/csp/gateway/am/api/loggedin/user/profile`;
  console.log(url);
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log(`/api/loggedin/user/profile - masuk pak eko!`);
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}

// https://vra.nashta.local/csp/gateway/am/api/loggedin/user
exports.getLoggedInUser = async (req, res, next) => {
  const url = `${process.env.URL}/csp/gateway/am/api/loggedin/user`;
  console.log(url);
  const { authorization } = req.headers;
  const options = {
    headers: {
      authorization
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));

    const { data } = await axios.get(url, options);
    console.log(`/api/loggedin/user - masuk pak eko!`);
    res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
}