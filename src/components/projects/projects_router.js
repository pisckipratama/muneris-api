const router = require('express').Router();
const { getAllProjects } = require('./projects_controller');

router.get('/projects', getAllProjects);

module.exports = router;
