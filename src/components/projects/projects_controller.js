const axios = require('axios');
const URL = `${process.env.URL}/iaas/api/projects`;
const asyncHandler = require('../../lib/middlewares/asyncHandler');

exports.getAllProjects = async (req, res) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return res.status(401).json({ error: true, message: 'Unauthorize user!' });
    const { data } = await axios.get(URL, option);
    console.log('/api/projects - masuk pak eko!');
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};
