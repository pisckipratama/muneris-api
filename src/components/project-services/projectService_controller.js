const axios = require('axios');
const URL = `${process.env.URL}/project-service/api/projects`;
const asyncHandler = require('../../lib/middlewares/asyncHandler');
const queryString = require('querystring');
const ErrorResponse = require('../../lib/helpers/errorResponse');

exports.getAllProjectServices = async (req, res, next) => {
  const { authorization } = req.headers;
  const option = {
    headers: {
      authorization,
    },
  };

  try {
    if (!authorization)
      return next(new ErrorResponse('not allowed access / unauthorized', 401));
    const { data } = await axios.get(
      `${URL}?${queryString.stringify(req.query)}`,
      option
    );
    console.log(
      `/project-service/api/projects?${queryString.stringify(
        req.query
      )} - masuk pak eko!`
    );
    return res.status(200).json(data);
  } catch (error) {
    console.error(error.message);
    let respnose = { message: { error: 'an error occured' } };
    let code = 500;
    if (error.response) {
      respnose.message = error.response.data;
      code = error.response.status;
    }
    res.status(code).json(respnose.message);
  }
};
