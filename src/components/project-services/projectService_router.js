const router = require('express').Router();
const { getAllProjectServices } = require('./projectService_controller');

router.get('/project-service', getAllProjectServices);

module.exports = router;
