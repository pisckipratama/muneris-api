const ErrorHandler = require('../helpers/errorResponse');
const dotenv = require('dotenv');
dotenv.config();

const errorHandler = (err, req, res, next) => {
  let error = { ...err };

  error.message = err.message;
  if (err.response) {
    error = new ErrorHandler(error.message, err.response.data.statusCode);
    error.details = err.response.data;
  }

  if (err.message === `connect ETIMEDOUT ${process.env.ADDR}:443`) {
    const message = 'Bad Gateway';
    error = new ErrorHandler(message, 502);
  }

  // log to console
  console.error(err.stack.red);
  console.log(err.message);

  res.status(error.statusCode || 500).json({
    success: false,
    message: error.message || 'an error occurred.',
    details: error.details || {},
  });
};

module.exports = errorHandler;
