FROM node:12-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --silent

COPY env_example.list .env

COPY . .

EXPOSE 5001

CMD echo "192.168.1.202   vra.nashta.local" >> /etc/hosts; echo "192.168.1.201   vidm.nashta.local" >> /etc/hosts; npm start;